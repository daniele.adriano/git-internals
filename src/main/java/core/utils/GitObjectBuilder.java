package core.utils;

import core.AbstractGitObject;
import core.GitBlobObject;
import core.GitCommitObject;
import core.GitTreeObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static core.utils.FileUtils.UTF8;

/**
 * Created by daniele on 13/01/2018.
 */
public class GitObjectBuilder {

    private final static Logger log = LogManager.getLogger(GitObjectBuilder.class);
    private final static String OBJ_PATH = "objects";

    private String hash;
    private String repoPath;

    public GitObjectBuilder(String repoPath, String hash) {
        this.repoPath = repoPath;
        this.hash = hash;
    }

    public AbstractGitObject build() {
        String objFileName = null;
        AbstractGitObject gitObject = null;
        try {
            String objPath = hash.substring(0, 2);
            String objFile = hash.substring(2);
            objFileName = String.join("/", repoPath, OBJ_PATH, objPath, objFile);
            byte[] dec = FileUtils.readDecompressedContent(objFileName);

            ByteArrayOutputStream firstLineBuffer = new ByteArrayOutputStream();
            ByteArrayOutputStream contentBuffer = new ByteArrayOutputStream();
            int i = 0;
            // Read object first line. Byte 0 separate object first line from object content
            for (; dec[i] != 0; i++) {
                firstLineBuffer.write(dec[i]);
            }
            // skip byte 0 (first line / content separator)
            i++;
            // Read object content
            for (; i < dec.length; i++) {
                contentBuffer.write(dec[i]);
            }
            String firstLine = new String(firstLineBuffer.toByteArray(), UTF8);
            String type = StringUtils.substringBefore(firstLine, " ");
            int size = Integer.valueOf(StringUtils.substringAfter(firstLine, " "));
            byte[] content = contentBuffer.toByteArray();
            switch (type) {
                case "blob":
                    gitObject = new GitBlobObject(hash, type, size, new String(content, UTF8));
                    break;
                case "commit":
                    String[] conentLines = StringUtils.split(new String(content, UTF8), "\n");
                    String treeHash = StringUtils.substringAfter(conentLines[0], "tree ");
                    String parentHash = StringUtils.substringAfter(conentLines[1], "parent ");
                    String author = StringUtils.substringAfter(conentLines[2], "author ");
                    // Remove time zone offset
                    author = StringUtils.substringBeforeLast(author, " ");
                    // Remove unix timestamp
                    author = StringUtils.substringBeforeLast(author, " ");
                    gitObject = new GitCommitObject(hash, type, size, treeHash, parentHash, author);
                    break;
                case "tree":
                    Map<String, AbstractGitObject> entries = new HashMap<>();
                    for (i = 0; i < content.length; i++) {
                        ByteArrayOutputStream fileNameBuffer = new ByteArrayOutputStream();
                        ByteArrayOutputStream fileHashBuffer = new ByteArrayOutputStream();
                        for (; content[i] != 0; i++) {
                            fileNameBuffer.write(content[i]);
                        }
                        i++;
                        for (int j = 0; j < 20; i++, j++) {
                            fileHashBuffer.write(content[i]);
                        }
                        // Substring on filename to skip permission bytes
                        String fileName = StringUtils.substringAfter(new String(fileNameBuffer.toByteArray(), UTF8), " ");
                        String fileHash = DatatypeConverter.printHexBinary(fileHashBuffer.toByteArray()).toLowerCase();
                        GitObjectBuilder gob = new GitObjectBuilder(repoPath, fileHash);
                        entries.put(fileName, gob.build());
                    }
                    gitObject = new GitTreeObject(hash, type, size, entries);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown git object type " + type);
            }
        } catch (IOException e) {
            log.error("An error occurred opening file {}", objFileName);
        }
        return gitObject;
    }

}
