package core.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sun.reflect.misc.FieldUtil;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.InflaterInputStream;

/**
 * Created by daniele on 13/01/2018.
 */
public class FileUtils {

    public final static String UTF8 = "UTF-8";

    private final static Logger log = LogManager.getLogger(FieldUtil.class);

    public static byte[] readContentAsByte(String filePath) throws IOException {
        log.debug("Rading content of file {}", filePath);
        Path path = Paths.get(filePath);
        return Files.readAllBytes(path);
    }

    public static String readContentAsString(String filePath, String charset) throws IOException {
        return new String(readContentAsByte(filePath), charset);
    }

    public static String readContentAsStringUTF8(String filePath) throws IOException {
        return readContentAsString(filePath, UTF8);
    }

    public static String readFirstLineAsStringUTF8(String filePath) throws IOException {
        return readFirstLineAsString(filePath, UTF8);
    }

    public static String readFirstLineAsString(String filePath, String charset) throws IOException {
        log.debug("Rading first line of file {} with charset {}", filePath, charset);
        Path path = Paths.get(filePath);
        return Files.lines(path, Charset.forName(charset)).findFirst().get();
    }

    public static byte[] readDecompressedContent(String filePath) throws IOException {
        log.debug("Decompressing file {}", filePath);
        InflaterInputStream in = new InflaterInputStream(new FileInputStream(filePath));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int c;
        while ((c = in.read()) != -1) {
            out.write(c);
        }
        return out.toByteArray();
    }

}
