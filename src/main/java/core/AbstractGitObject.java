package core;

/**
 * Created by daniele on 13/01/2018.
 */
public abstract class AbstractGitObject {

    protected String hash;
    protected String type;
    protected int size;

    public AbstractGitObject(String hash, String type, int size) {
        this.hash = hash;
        this.type = type;
        this.size = size;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
