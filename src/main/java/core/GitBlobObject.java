package core;

/**
 * Created by daniele on 13/01/2018.
 */
public class GitBlobObject extends AbstractGitObject {

    private String content;

    public GitBlobObject(String hash, String type, int size, String content) {
        super(hash, type, size);
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
