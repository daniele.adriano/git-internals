package core;

import java.util.Collection;
import java.util.Map;

/**
 * Created by daniele on 13/01/2018.
 */
public class GitTreeObject extends AbstractGitObject {

    private Map<String, AbstractGitObject> entries;

    public GitTreeObject(String hash, String type, int size, Map<String, AbstractGitObject> entries) {
        super(hash, type, size);
        this.entries = entries;
    }

    public AbstractGitObject getEntry(String type) {
        return entries.get(type);
    }

    public Collection getEntryPaths() {
        return entries.keySet();
    }
}
