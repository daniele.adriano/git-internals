package core;

/**
 * Created by daniele on 13/01/2018.
 */
public class GitCommitObject extends AbstractGitObject {

    private String treeHash;
    private String parentHash;
    private String author;

    public GitCommitObject(String hash, String type, int size, String treeHash, String parentHash, String author) {
        super(hash, type, size);
        this.treeHash = treeHash;
        this.parentHash = parentHash;
        this.author = author;
    }

    public String getTreeHash() {
        return treeHash;
    }

    public void setTreeHash(String treeHash) {
        this.treeHash = treeHash;
    }

    public String getParentHash() {
        return parentHash;
    }

    public void setParentHash(String parentHash) {
        this.parentHash = parentHash;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

}
