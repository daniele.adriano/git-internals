package core;

import core.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Created by daniele on 13/01/2018.
 */
public class GitRepository {

    private final static Logger log = LogManager.getLogger(GitRepository.class);

    private String repoPath;

    public GitRepository(String repoPath) {
        if (StringUtils.isEmpty(repoPath)) {
            throw new IllegalArgumentException("Repository path cannot be null or empty");
        }
        this.repoPath = repoPath;
    }

    public String getHeadRef() {
        String headRef = null;
        String headFileName = String.join("/", repoPath, "HEAD");
        try {
            headRef = FileUtils.readFirstLineAsStringUTF8(headFileName);
            headRef = headRef.replaceAll("ref: ", "");
        } catch (IOException e) {
            log.error("An error occurred opening file {}", headFileName);
        }
        return headRef;
    }

    public String getRefHash(String refSubPath) {
        String refHash = null;
        String refFileName = String.join("/", repoPath, refSubPath);
        try {
            refHash = FileUtils.readFirstLineAsStringUTF8(refFileName);
        } catch (IOException e) {
            log.error("An error occurred opening file {}", refFileName);
        }
        return refHash;
    }

    public String getRepoPath() {
        return repoPath;
    }
}
